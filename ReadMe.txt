TUTORIAL
========

	How to create a CXF service (WSDL-first)
	(from scratch, with a text editor, based on Fuse 6.2.1)

	Index:
	-----
		1. Implementation and Deployment in Fuse
		2. Running standalone (and testing)
		3. Alternative CXF implementation
		4. Defining a CXF producer (client)
			1. sending simple data
			2. sending complex data
			3. CXF wrapped/unwrapped modes
		5. Example using the SoapJaxb DataFormat


1. Implementation and Deployment
   (and testing using SoapUI)
=================================

1.1 Create skeleton

	mvn archetype:generate                   \
	  -DarchetypeGroupId=org.apache.camel.archetypes  \
	  -DarchetypeArtifactId=camel-archetype-blueprint \
	  -DarchetypeVersion=2.15.1.redhat-621084  \
	  -DgroupId=learn.cxf                  \
	  -DartifactId=cxf-wsdl-first

	(there is a bug in the JUnit, remove the slash in "/OSGI-INF...")

	Note: the archetype will generate some code that may enter in conflict
		  with our tutorial, simply remove the non-relevant code.


1.2 Create a WSDL and place under:

	> src/main/resources/wsdl

	You might want to reuse the WSDL generated from the 'cxd-code-first' example.
	Just make sure the Hello Interface and Bean java classes generated from the archetype do not clash:

		a. rename Hello.java --> SayHello.java
		b. rename HelloBean.java --> SayHelloBean.java
		c. update references from the 'blueprint.xml'


1.3 Add the WSDL2java plugin to generate the sources

	  note: the property 'cxf.version' is set to '3.0.4.redhat-621084'
	  note: the sources will be generated in the 'target', this ensure 'src' stays clean.

	  ...
		<camel.version>2.15.1.redhat-621084</camel.version>
		<cxf.version>3.0.4.redhat-621084</cxf.version>
	  </properties>

	  <plugin>
		<groupId>org.apache.cxf</groupId>
		<artifactId>cxf-codegen-plugin</artifactId>
		<version>${cxf.version}</version>
		<executions>
		  <execution>
			<id>generate-sources</id>
			<phase>generate-sources</phase>
			<configuration>
			  <sourceRoot>${basedir}/target/generated/src/main/java</sourceRoot>
			  <wsdlOptions>
				<wsdlOption>
				  <wsdl>${basedir}/src/main/resources/wsdl/Hello.wsdl</wsdl>
				</wsdlOption>
			  </wsdlOptions>
			</configuration>
			<goals>
			  <goal>wsdl2java</goal>
			</goals>
		  </execution>
		</executions>
	  </plugin>


1.4 Add the endpoint in Blueprint

	a. Add the namespace:  
		xmlns:camelcxf="http://camel.apache.org/schema/blueprint/cxf"

	b. and the endpoint:

		<camelcxf:cxfEndpoint id="mycxf"
			 address="http://localhost:10000/mycxfserver/hello"
			 endpointName="ns:HelloWSPort"
			 serviceName="ns:HelloService"
			 wsdlURL="wsdl/Hello.wsdl"
			 serviceClass="learn.cxf.HelloWS"
			 xmlns:ns="http://cxf.learn/"/>

		Notes about 'address':
			- its value will overwrite the one defined in the WSDL's port.
			- it requires 'host:port' if run standalone (when testing or with 'camel:run')
			- when deployed in Fuse, you can omit 'host:port' and it will default to:
				> http://localhost:8181/cxf/mycxfserver/hello


1.5 Add the Camel CXF route to consume traffic

		<route id="my-cxf-route">
			<from uri="cxf:bean:mycxf"/>
			<log message="got message ${body}"/>
			<setBody>
				<simple>Hello ${body}</simple>
			</setBody>
		</route>


1.6 Install in Maven and deploy in Fuse (and start)

	a. mvn clean install -DskipTests=true

	b. JBossFuse:karaf@root> install -s mvn:learn.cxf/cxf-wsdl-first/1.0.0


1.7 From a browser, check it got deployed:

	With the address:
	http://localhost:8181/cxf

	It should show:
	Available SOAP services:
	HelloWS
	    hello
		Endpoint address: http://localhost:10000/mycxfserver/hello
		WSDL : {http://cxf.learn/}HelloService
		Target namespace: http://cxf.learn/


1.8 From SoapUI, you should be able to test it:

	a. Select: File > New Soap Project
	b. Give it a name  
	c. Enter 'Initial WSDL' as:
		> http://localhost:10000/mycxfserver/hello?wsdl



2. Testing JUnits and running standalone
========================================

	JBoss Fuse (the engine) already contains all the dependencies required by the project.
	But the project won't run without a number of dependencies when:
		a. running JUnits (maven test phase)
		b. running standalone (with 'mvn camel:run')


2.1 The POM file needs some CXF dependencies:

	a. add dependency ASM at the top of <dependencies>

		<dependency>
		  <groupId>org.ow2.asm</groupId>
		  <artifactId>asm-all</artifactId>
		  <version>4.1</version>
		</dependency>

		(ref: https://access.redhat.com/solutions/1128593)

	b. add CXF dependencies:

		...
			<camel.version>2.15.1.redhat-621084</camel.version>
			<cxf.version>3.0.4.redhat-621084</cxf.version>
		</properties>
		...
		<dependency>
		    <groupId>org.apache.camel</groupId>
      		<artifactId>camel-cxf</artifactId>
		    <version>${camel.version}</version>
		</dependency>
		<dependency>
		    <groupId>org.apache.cxf</groupId>
		    <artifactId>cxf-rt-transports-http</artifactId>
		    <version>${cxf.version}</version>
		</dependency>
		<dependency>
		    <groupId>org.apache.cxf</groupId>
		    <artifactId>cxf-rt-transports-http-jetty</artifactId>
		    <version>${cxf.version}</version>
		</dependency>

2.2 To run standalone:

	a. run the command:
		> mvn clean camel:run

	b. from SoapUI you should be able to hit the service via the URL:
		> http://localhost:10000/mycxfserver/hello


2.3 To run Junits

	a. add the following JUnit that uses the CXF endpoint to send a WS request:

		@Test
		public void testRouteCxf() throws Exception{
			Object response = template.sendBody("cxf:bean:mycxf", ExchangePattern.InOut, "junit");
			assertEquals("ups", "[Hello junit]", response.toString());
		}

		Note: CXF returns a list where the response is contained, hence the brackets in the assertion. 

	b. run the maven command:
		> mvn clean test


3. Alternative implementation for the CXF consumer
==================================================

	It might be very interesting to decouple the CXF processing from the transport layer.
	Some reasons why this might be useful:
		> gain more control over the HTTP layer
		> use other transports other than HTTP


3.1 Implementation

	As an example, we could implement the route using the transport Netty as follows:

		<route id="my-netty-cxf-route">
			<from uri="netty-http:http://localhost:11000/mycxfserver/hello"/>
			<to uri="cxfbean:sayhellobean"/>
		</route>

	where 'sayhellobean' is defined in blueprint as follows:
		
		<bean id="sayhellobean" class="learn.cxf.SayHelloBean"/>


	The interface 'SayHello' would look as follows:

		----------------------------------
		import javax.jws.*;
		@WebService(name="HelloWS")
		public interface SayHello {
			String hello(@WebParam(name="thename") String thename);
		}
		----------------------------------
	
	The implementation class 'HelloBean' would look as follows:

		----------------------------------
		@javax.jws.WebService(endpointInterface="learn.cxf.SayHello")
		public class SayHelloBean implements SayHello{
			public String hello(String thename){
				System.out.println("SayHelloBean bean got message: " + thename);
				return "Hello " + thename;
			}
		}
		----------------------------------

	The use of Camel Netty Http would need to include the Maven dependency:

		<dependency>
		  <groupId>org.apache.camel</groupId>
		  <artifactId>camel-netty-http</artifactId>
		  <version>${camel.version}</version>
		</dependency>


3.2 To run standalone:

	a. run the command:
		> mvn clean camel:run

	b. you could equally obtain the WSDL via
		> http://localhost:11000/mycxfserver/hello?wsdl

	c. from SoapUI you should be able to hit the service via the URL:
		> http://localhost:11000/mycxfserver/hello


3.3 To run Junits

	a. add the following JUnit to send a WS request:

		@Test
		public void testRouteNetty() throws Exception
		{
			//prepare new request to destination Netty endpoint
			DefaultExchange request = new DefaultExchange(context);
			request.getIn().setHeader("CamelDestinationOverrideUrl", "http://localhost:11000/mycxfserver/hello");
			request.getIn().setBody("junit");
		
			//trigger the request via CXF to Netty
			Exchange response = template.send("cxf:bean:mycxf", request);

			//expect the 'hello' message
			assertEquals("ups", "Hello junit", response.getOut().getBody(String.class));
		}

	b. run the maven command:
		> mvn clean test
	

============================================================
============================================================


4. How to create a CXF client (POJO)
====================================

	Reference:
	The most useful source of information I could find on how to prepare CXF invocations was from:

		> https://access.redhat.com/documentation/en-US/Red_Hat_JBoss_Fuse/6.0/html/Web_Services_and_Routing_with_Camel_CXF/files/Pojo.html


4.1 Simple operation invocation

	a. The current definition

		So far the WSDL definition has only a 'hello' service with a single parameter:

			<wsdl:message name="hello">
			  <wsdl:part name="parameters" element="tns:hello">
			  </wsdl:part>
			</wsdl:message>


	b. Defining a Camel CXF producer

		When there is only one service/operation definition, Camel CXF will default to it.
		Camel CXF will automatically map the body to the parameter of the method 'hello'.

		A sample route invoking the WS service could be:

			<route id="call-simple-data">
				<from uri="direct:trigger-call-simple-data"/>
				<setBody>
					<constant>cxf-client</constant>
				</setBody>
				<to uri="cxf:bean:mycxf"/>
				<log message="The response contains [${body}]"/>
			</route>

		The server is implemented as

			<from uri="cxf:bean:mycxf?dataFormat=POJO"/>
			<setBody><simple>Hello ${body}</simple></setBody>

		Invoking the route 'direct:trigger-call-simple-data' would show the following output:

			INFO  The response contains [Hello cxf-client]


4.2 Another operation in the WSDL with complex data

	a. The complex data definition

		We extend the WSDL/XSD/XML definition to include a second opearation called 'bonjour'.
		The WSDL needs then to include a new Message, PortType operation and Binding opearation.

		And the new XML schema itmes to be included would be: 

		  <xs:element name="bonjour" type="tns:bonjour"/>
		  <xs:element name="bonjourResponse" type="tns:bonjourResponse"/>

		  <xs:complexType name="bonjour">
			<xs:sequence>
			  <xs:element minOccurs="0" name="input1" type="xs:string"/>
			  <xs:element minOccurs="0" name="input2" type="tns:bonjourSubnode"/>
			  <xs:element minOccurs="0" name="input3" type="xs:string"/>
			</xs:sequence>
		  </xs:complexType>

		  <xs:complexType name="bonjourSubnode">
			<xs:sequence>
			  <xs:element minOccurs="0" name="data1" type="xs:string"/>
			  <xs:element minOccurs="0" name="data2" type="xs:string"/>
			</xs:sequence>
		  </xs:complexType>

		  <xs:complexType name="bonjourResponse">
			<xs:sequence>
			  <xs:element minOccurs="0" name="return" type="xs:string"/>
			</xs:sequence>
		  </xs:complexType>


	b. Defining a Camel CXF producer

		In this case the operation 'bonjour' requires three input parameters (input1,input2, input3).
		Camel CXF requires to be passed a list that includes all three parameters.

		For this we could construct the list bean in Blueprint as follows:

			<bean id="input1" class="java.lang.String">
				<argument value="Alexander"/>
			</bean>

			<bean id="input2" class="learn.cxf.BonjourSubnode">
				<property name="data1" value="the"/>
				<property name="data2" value="Great"/>
			</bean>

			<bean id="inputList" class="java.util.ArrayList">
			  <argument>
				<list>
				  <ref component-id="input1"/>
				  <ref component-id="input2"/>
				  <value>King of Macedonia</value>
				</list>
			  </argument>
			</bean>

			(of course the same construct could be done in Java)

		When the WSDL defines more than one operation then Camel CXF requires to be told which one to invoke.
		The list needs to be set in the body for Camel CXF to pick the parameters.
		
		A sample route that would invoke the WS service would be:

			<route id="call-complex-data">
				<from uri="direct:call-complex-data"/>
				<setBody>
					<simple>ref:inputList</simple>
				</setBody>
				<to uri="cxf:bean:mycxf?defaultOperationName=bonjour"/>
				<log message="The response contains [${body}]"/>
			</route>

		Note the operation 'bonjour' is specified as a query parameter.
		By default the data format is POJO, there is no need to include the option.

		The server is re-implemented as: (compatible for both operations 'hello' and 'bonjour')

			<route id="my-cxf-route">
				<from uri="cxf:bean:mycxf"/>
				<log message="opeartion [${header.operationName}], got message ${body}"/>
				<choice>
					<when>
						<simple>${header.operationName} == 'bonjour'</simple>
						<setBody>
							<simple>Bonjour ${body[0]} ${body[1].data1} ${body[1].data2}, ${body[2]}</simple>
						</setBody>
					</when>
					<otherwise>
						<setBody>
							<simple>Hello ${body}</simple>
						</setBody>
					</otherwise>
				</choice>
			</route>

		The execution would show the following output:

			INFO  The response contains [Bonjour Alexander the Great, King of Macedonia]


4.3 Wrapped and Unwrapped invocations.

	(references:
		> http://cxf.apache.org/docs/http-binding.html
		> http://camel.apache.org/cxf.html
    )

	The previous example showed how to send complex structures (multiple nodes) using CXF.
	CXF runs by default in 'unwrapped' mode (wrapped=false) which requires a List of parameters.
	Setting the option 'wrapped = true' allows to set as input the POJO from its root.
	Here below it is shown how we could invoke the operation 'bonjour' without having to construct a List.

	Instead of a list we define the following bean:

		<bean id="bonjour" class="learn.cxf.Bonjour">
		  <property name="input1" ref="input1"/>
		  <property name="input2" ref="input2"/>
		  <property name="input3" value="King of Macedonia"/>
		</bean>

		(of course the same construct could be done in Java)


	A sample route that would invoke the WS service would be:

		<route id="call-complex-data-wrapped">
			<from uri="direct:call-complex-data-wrapped"/>
			<setBody>
				<simple>ref:bonjour</simple>
			</setBody>
			<to uri="cxf:bean:mycxf?defaultOperationName=bonjour&amp;wrapped=true"/>
			<log message="(wrapped) The response contains [${body}]"/>
		</route>

		Note the CXF invocation includes the query parameter 'wrapped=true' to indicate the input is wrapped.

	The execution would show the following output:

		INFO  (wrapped) The response contains [hello Alexander the Great]


============================================================
============================================================


5. Example using the SoapJaxb DataFormat
=============================================

	It might come handy on some use cases to make use of the SoapJaxb DataFormat.
	It's meant to transform SOAP to POJO, and POJO to SOAP in a very easy way.

	Include in the POM the following dependency:

		<dependency>
		  <groupId>org.apache.camel</groupId>
		  <artifactId>camel-soap</artifactId>
		  <version>${camel.version}</version>
		</dependency>

	The data format needs to be declared in the Blueprint file inside the 'CamelContext' as:

		<dataFormats>
			<soapjaxb id="soap12" contextPath="learn.cxf" version="1.2"/>
		</dataFormats>

	The 'contextPath' tells the dataformat where to find the Java classes generated from the WSDL definition.

	The example shown below comprises of two Camel Routes:
		
		- One, to transform POJO to SOAP and persist on file (soap-request.xml)
		- Two, to read from file, convert to POJO and invoke the WS service via CXF

	The Camel definitions would look as follows:

		<route id="persist-soap">
			<from uri="direct:trigger-persist-soap"/>
			<setBody>
				<simple>ref:bonjour</simple>
			</setBody>
			<marshal ref="soap12"/>
			<log message="SOAP to persist: ${body}"/>
			<to uri="file:data?fileName=soap-request.xml"/>
		</route>

		<!-- sample to convert from SOAP to POJO and call the WS service-->
		<route id="recover-soap-and-call-cxf">
			<from uri="file:data?fileName=soap-request.xml"/>
			<log message="recovered SOAP from file: ${body}"/>
			<unmarshal ref="soap12"/>
			<to uri="cxf:bean:mycxf?defaultOperationName=bonjour&amp;wrapped=true"/>
		  	<to uri="mock:recover-soap"/>
		</route> 

	Where the bean 'bonjour' is based on the defintion from section 4.3, and the DSLs 'marshal/unmarshal' are the transforming actions.

	This example can be triggered from a JUnit with the following instruction:

		template.sendBody("direct:trigger-persist-soap","");


