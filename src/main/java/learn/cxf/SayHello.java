package learn.cxf;

import javax.jws.*;

@WebService(name="HelloWS")
public interface SayHello {
	String hello(@WebParam(name="thename") String thename);
}
