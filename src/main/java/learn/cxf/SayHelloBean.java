package learn.cxf;

@javax.jws.WebService(endpointInterface="learn.cxf.SayHello")
public class SayHelloBean implements SayHello{
	public String hello(String thename){
		System.out.println("SayHelloBean bean got message: " + thename);
		return "Hello " + thename;
	}
}

